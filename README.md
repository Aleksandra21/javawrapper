# JavaWrapper
This application is made for the classification of birds into different ecological groups based on their bone measurements

## Prerequisites
This is what you need to run this application:
Java 1.8.0

## Installing
Clone the repository by copying and pasting the text below in your terminal:
git clone https://bitbucket.org/Aleksandra21/javawrapper.git

## Usage
!Before running any code, please change the path in line 78 Wekarunner.java to your own path to get the outputfile!

## In intelij:
Add configurations:
Example: -i "0.5607,0.6428,0.4799,0.6379,0.5508,0.5465,0.0000,0.6036,0.5155,0.5753,?"

-i "Path of the project or single input comma seperated without spaces and last class attribute as ?" 

-m "Path to model file" 

-o "Path to outputfile" is optional

Then run the CommandLineArgsRunner.

## In commandline:
-Go to the directory that contains this project.

-Type in: java CommandLineArgsRunner.java with the following arguments:

-i ["Path of the project or single input comma seperated without spaces and last class attribute as ?" ]

-m ["Path to model file"] 

-o ["Path to outputfile" is optional]


## Author
Aleksandra Kapielska

## License
/ Copyright (c) 2019 <Aleksandra Kapielska>. Licensed under GPLv3. See gpl.md /