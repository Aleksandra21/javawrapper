package nl.bioinf.arguments_provider;

import weka.classifiers.AbstractClassifier;
import weka.classifiers.lazy.IBk;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ConverterUtils.DataSource;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


/*
This class loads the classifier and classifies the instances of the given file
also validates the given files
 */
public class WekaRunner {
    private String modelFile;
    private String unknownFile;
    private String outputFile;
    private String literalInput;


    /**
     * Reads the input files and starts the weka wrapper.
     */
    public WekaRunner(String file, String output, String model) throws Exception {
        this.unknownFile = file;
        this.outputFile = output;
        this.modelFile = model;

        System.out.println("start");
        try {
            start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method checks model and arff file on extension and emptyness
     * after that it loads the classifier and prints output- and unknown file
     */
    private void start() throws Exception {
        /*String modelFile = "/Users/aleksandra/Desktop/javawrapper/src/main/java/nl/bioinf/arguments_provider/MyModel.model";
        String unknownFile = "/Users/aleksandra/Desktop/javawrapper/src/main/java/nl/bioinf/arguments_provider/Unknown_bird.arff";
        String outputFile = "/Users/aleksandra/Desktop/javawrapper/src/main/java/nl/bioinf/arguments_provider/Output.txt";*/
        if (unknownFile.matches("([0-9]?\\.[0-9]*,){10}\\?")) {
            String inputFilename = singleInputToProcess(unknownFile);
            IBk fromFile = loadClassifier();
            Instances unknownInstances = loadArff(inputFilename);
            System.out.println("\nunclassified unknownInstances = \n" + unknownInstances);
            Instances labels = classifyNewInstance(fromFile, unknownInstances);
        } else {
            try {
                isLegalPath(unknownFile);
                isLegalPath(modelFile);
                hasExtension(unknownFile, ".arff");
                hasExtension(modelFile, ".model");
                fileIsEmpty(unknownFile);
                fileIsEmpty(modelFile);
                IBk fromFile = loadClassifier();
                Instances unknownInstances = loadArff(unknownFile);
                System.out.println("\nunclassified unknownInstances = \n" + unknownInstances);
                Instances labels = classifyNewInstance(fromFile, unknownInstances);
                writeFile("/Users/aleksandra/Desktop/JavaBitbucket/javawrapper3/src/main/java/nl/bioinf/arguments_provider/outputfile.txt", labels);
                System.out.println(labels);
                System.out.println("Written to file: ../outputfile.txt");
            } catch (Exception e) {
                throw new IllegalArgumentException(e.getMessage());
            }
        }
    }

    /**
     * Writes the output file
     */
    private void writeFile(String outputFile, Instances labels) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));

        writer.write(labels.toString());
        writer.close();
    }

    /**
     * Stores the single line commandline input for further classification
     */
    private String singleInputToProcess(String literalInput) throws IOException {
        String literalInputPath = "../javawrapper3/src/main/java/nl/bioinf/arguments_provider/singleinput.arff";
        String arffEssentials = "@relation bird.bones.measurements\n" + "\n" + "@attribute huml numeric\n" +
                "@attribute humw numeric\n" +
                "@attribute ulnal numeric\n" +
                "@attribute ulnaw numeric\n" +
                "@attribute feml numeric\n" +
                "@attribute femw numeric\n" +
                "@attribute tibl numeric\n" +
                "@attribute tibw numeric\n" +
                "@attribute tarl numeric\n" +
                "@attribute tarw numeric\n" +
                "@attribute type {SW, W, T, R, P, SO}\n"
                + "\n" + "@data\n";
        BufferedWriter writer = new BufferedWriter(new FileWriter(literalInputPath, false));
        writer.write(arffEssentials);
        writer.append(literalInput);
        writer.close();
        return literalInputPath;


    }


    /**
     * This method classifies instances
     */
    private Instances classifyNewInstance(IBk model, Instances unknownInstances) throws Exception {
        // create copy
        Instances labeled = new Instances(unknownInstances);
        // label instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            double clsLabel = model.classifyInstance(unknownInstances.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
        }
        System.out.println("\nNew, labeled = \n" + labeled);
        //writeFile("/Users/aleksandra/Desktop/JavaBitbucket/javawrapper3/src/main/java/nl/bioinf/arguments_provider/outputfile.txt", labeled);
        return labeled;
    }

    /**
     * This method loads the classifier
     */
    private IBk loadClassifier() throws Exception {
        // deserialize model
        return (IBk) weka.core.SerializationHelper.read(modelFile);
    }

//    /**
//     *This method saves the classifier
//     */
//    private void saveClassifier(IBk iBk) throws Exception {
//        //post 3.5.5
//        // serialize model
//        weka.core.SerializationHelper.write(modelFile, iBk);
//    }

    /**
     * Gets the instances
     */
    private void printInstances(Instances instances) {
        int numAttributes = instances.numAttributes();

        for (int i = 0; i < numAttributes; i++) {
            System.out.println("attribute " + i + " = " + instances.attribute(i));
        }

        System.out.println("class index = " + instances.classIndex());

        int numInstances = instances.numInstances();
        for (int i = 0; i < numInstances; i++) {
            if (i == 5) break;
            Instance instance = instances.instance(i);
            System.out.println("instance = " + instance);
        }
    }

    /**
     * This method loads the arff file
     */
    private Instances loadArff(String datafile) throws IOException {
        try {
            DataSource source = new DataSource(datafile);
            Instances data = source.getDataSet();

            // setting class attribute if the data format does not provide this information
            // For example, the XRFF format saves the class attribute information as well
            if (data.classIndex() == -1)
                data.setClassIndex(data.numAttributes() - 1);
            return data;
        } catch (Exception e) {
            throw new IOException("could not read from file");
        }
    }


    private void isLegalPath(String file) {
        File in = new File(file);
        if (in.exists()) {
            System.out.print("");
        } else {
            throw new IllegalArgumentException("File(path) does not exist :( ");
        }
    }

    /**
     * Throws exception if file is empty
     *
     * @param file
     */
    private void fileIsEmpty(String file) {
        File in = new File(file);
        if (in.length() == 0) {
            throw new IllegalArgumentException("File is empty!");
        }
    }


    /**
     * Throws an exception if file is not an arff file
     *
     * @param file
     */
    private void isArffFile(String file) {
        hasExtension(file, ".arff");
    }

    /**
     * This method throws an checks the given extention of files and
     * throws an exception if extention doesn't match
     *
     * @param file, extension
     */

    private void hasExtension(String file, String extension) {
        if (!file.endsWith(extension)) {
            throw new IllegalArgumentException("This is not a " + extension + " file.");
        }
    }

    public String getUnknownFile() {
        return unknownFile;
    }
}

 
 


