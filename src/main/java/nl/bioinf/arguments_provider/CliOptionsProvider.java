package nl.bioinf.arguments_provider;


import org.apache.commons.cli.*;

public class CliOptionsProvider implements OptionsProvider {
    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;
    private final String HELP = "help";
    private final String INPUT = "inputFile";
    private final String OUTPUT = "outputFile";
    private final String MODEL = "modelFile";

    public CliOptionsProvider(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    /**
     * Options initialization and processing.
     */
    private void initialize() {
        buildOptions();
        processCommandLine();
    }

    /**
     * Checks if user needs the help option; if so, return true.
     *
     * @return helpRequested
     */
    public boolean helpRequested() {
        return this.commandLine.hasOption(HELP);
    }


    /**
     * This method creates and adds options for commnandline
     */
    private void buildOptions() {
        this.options = new Options();
        Option helpOption = new Option("h", HELP, false, "Prints help message");
        Option InputOption = new Option("i", INPUT, true, "Path to input file or single input");
        Option OutputOption = new Option("o", OUTPUT, false, "Name of output file");
        Option ModelOption = new Option("m", MODEL, true, "Path to model file");


        options.addOption(helpOption);
        options.addOption(InputOption);
        options.addOption(OutputOption);
        options.addOption(ModelOption);


    }

    /**
     * This method parses the commandline arguments
     */
    private void processCommandLine() {
        try {

            CommandLineParser parser = new DefaultParser();
            this.commandLine = parser.parse(this.options, this.clArguments);

            if (commandLine.hasOption(INPUT)) {
                String s = commandLine.getOptionValue(INPUT).trim(); //sets the input option
            } else {
                System.out.println("Error in input (file)");
            }
            if (commandLine.hasOption(MODEL)) {
                String m = commandLine.getOptionValue(MODEL).trim(); //sets the model option
            } else {
                System.out.println("Error in input (model)");
            }
            if (commandLine.hasOption(OUTPUT)) {
                String o = commandLine.getOptionValue(OUTPUT); //sets the output option
            } else {
                System.out.println("Error in input (output)");
            }

        } catch (ParseException e) {
            throw new IllegalArgumentException("Your commandline was empty or you did not fill in an argument");
        }


    }


    /**
     * Prints help message when asked for
     */
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Print help ", options);
    }


    @Override
    public String getInputFile() {
        return this.commandLine.getOptionValue(INPUT);
    }

    @Override
    public String getModelFile() {
        return this.commandLine.getOptionValue(MODEL);
    }

    @Override
    public String getOutputFile() {
        return this.commandLine.getOptionValue(OUTPUT);
    }

}




