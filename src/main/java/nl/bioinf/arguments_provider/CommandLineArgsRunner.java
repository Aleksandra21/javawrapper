package nl.bioinf.arguments_provider;

/**
 * This Main class works with user input commandline arguments that are given and parsed by CliOptionsProvider.
 * The class is set to final because it's not designed for further extension.
 */

public final class CommandLineArgsRunner {
    private CommandLineArgsRunner() {

    }

    public static void main(String[] args) throws Exception {
        try {
            CliOptionsProvider op = new CliOptionsProvider(args);
            System.out.println(op.getInputFile());
            System.out.println(op.getOutputFile());
            System.out.println(op.getModelFile());

/*            String modelFile = "/Users/aleksandra/Desktop/javawrapper/src/main/java/nl/bioinf/arguments_provider/MyModel.model";
            String unknownFile = "/Users/aleksandra/Desktop/javawrapper/src/main/java/nl/bioinf/arguments_provider/Unknown_bird.arff";
            String outputFile = "/Users/aleksandra/Desktop/javawrapper/src/main/java/nl/bioinf/arguments_provider/Output.txt";
            WekaRunner w = new WekaRunner(unknownFile, outputFile, modelFile);*/

            if (op.helpRequested()) {
                op.printHelp();
            }
            else if (op.getInputFile()!= null) {
                WekaRunner w = new WekaRunner(op.getInputFile(), op.getOutputFile(), op.getModelFile());
            }
            else {
                System.out.println("Error parsing input");
            }

            /*
            TO DO:
            1) path fix
            2) Input checks / valid extension etc
            3) single line input/output
            4) comments
            */
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }

    }
}
