package nl.bioinf.arguments_provider;

/**
 * This interface specifies options for the options provider
 */
    public interface OptionsProvider {
    /**
     * Serves the InputFile to be used.
     *
     * @return InputFilePath
     */
    String getInputFile();


    /**
     * Serves the ModelFile.
     *
     * @return ModelFile
     */
    String getModelFile();


    /**
     * Serves the OutputFile.
     *
     * @return OutputFile
     */
    String getOutputFile();

}

